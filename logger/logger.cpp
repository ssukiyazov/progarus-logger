/*
 * logger.cpp
 *
 *  Created on: Jul 30 2020
 *      Author: Sergey A. Sukiyazov <sergey.sukiyazov@gmail.com>
 *     License: The 3-Clause BSD License
 *
 *--------------------------------------------------------------------------------------------------
 * Copyright 2020 Sergey A. Sukiyazov
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted
 * provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions
 *    and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions
 *    and the following disclaimer in the documentation and/or other materials provided with the
 *    distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse
 *    or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *--------------------------------------------------------------------------------------------------
 */
#include "logger.h"

#include <iostream>
#include <memory>
#include <stdexcept>
#include <algorithm>
#include <chrono>
#include <iterator>
#include <cstddef>
#include <cstdlib>
#include <cstring>
#include <cinttypes>

#if defined(WIN32)
# if defined(_MSC_VER)
#  include <string.h>
# endif // defined(_MSC_VER)
using ssize_t = long int;
#endif // defined(WIN32)

//-- static global data ----------------------------------------------------------------------------

namespace pgr {

static logger::settings s_settings;

//-- static helper functions -----------------------------------------------------------------------

static std::string string_format(const std::string &format, va_list ap) {
    std::string result;
    int size;

    va_list ap_copy;
    va_copy(ap_copy, ap);

    size = std::vsnprintf(nullptr, 0, format.c_str(), ap);
    if (size > 0) {
        result.resize(size + 1, char(0));
        std::vsnprintf(&result[0], size + 1, format.c_str(), ap_copy);
    }

    va_end(ap_copy);
    return result;
}

static bool proceed_with_settings(const pgr::logger::settings &settings, pgr::logger::level level, const std::string &message, const pgr::logger::context &context) {
    // don't flush messages if we have logging turned off
    if (settings.default_level == pgr::logger::level::off)
        return true;

    // check default logging level and don't flush messages if we it's level less than default
    if (settings.default_level != pgr::logger::level::all && level < settings.default_level)
        return true;

    // check if we have global callback and then uses it
    if (settings.callback) {
        settings.callback(level, message, context);
        return true;
    }

    // we didn't handle message here
    return false;
}

inline static bool is_level_enabled_helper(pgr::logger::level _default_level, pgr::logger::level _level) {
    if (_default_level == pgr::logger::level::all)
        return true;

    if (_default_level == pgr::logger::level::off || _default_level > _level)
        return false;

    return true;
}

//-- pgr::logger::message --------------------------------------------------------------------------

class logger::message {
public:
    message() : m_level(logger::level::null) {}
    message(const message &other) = default;
    message &operator=(const message &other) = default;

    message(logger::level level) : message(level, std::string()) {}
    message(logger::level level, const std::string &text) : m_level(level), m_stream(std::make_shared<std::stringstream>()) {
        if (!text.empty() && m_stream)
            m_stream->str(text);
    }

    bool is_valid() const { return m_level > logger::level::null && m_stream; }
    bool empty() const { return !is_valid() || m_stream->str().empty(); }

    logger::level level() const { return m_level; }

    std::stringstream &stream() { return *m_stream; }
    std::string str() const {
        if (!m_stream)
            return std::string();
        return m_stream->str();
    }

    void append(const std::string &str) {
        if (!m_stream)
            return;
        std::copy(str.cbegin(), str.cend(), std::ostream_iterator<std::stringstream::char_type>(*m_stream));
    }

    void flush_to_stdout(const std::string &category) {
        if (!is_valid() || empty())
            return;

        std::ostream &os = (m_level > pgr::logger::level::info ? std::cerr : std::cout);
        const std::string &str = m_stream->str();

        if (!category.empty())
            os << category << ":";

        os << level_to_name(m_level) << ": ";

        // std::copy(str.cbegin(), str.cend(), std::ostream_iterator<std::ostream::char_type>(os));
        os << str << std::endl;
        os.flush();
    }

    void update_context(logger::context &ctx) const {
        ctx.at = m_at;
        ctx.thread_id = m_thread_id;
    }

private:
    logger::level                      m_level;
    std::shared_ptr<std::stringstream> m_stream;
    std::thread::id                    m_thread_id = std::this_thread::get_id();
    logger::clock::time_point          m_at = std::chrono::steady_clock::now();
};

//-- class pgr::logger::settings -------------------------------------------------------------------


//-- class pgr::logger -----------------------------------------------------------------------------

std::string pgr::logger::level_to_name(level _level) {
    switch (_level) {
        case level::off:     return "off";
        case level::all:     return "all";
        case level::null:    return "null";
        case level::trace:   return "trace";
        case level::debug:   return "debug";
        case level::info:    return "info";
        case level::warning: return "warning";
        case level::error:   return "error";
        case level::fatal:   return "fatal";
        default: break;
    }
    return std::string();
}

logger::level logger::name_to_level(const std::string &name) {
    if (name == "trace")   return level::trace;
    if (name == "debug")   return level::debug;
    if (name == "info")    return level::info;
    if (name == "warning") return level::warning;
    if (name == "error")   return level::error;
    if (name == "fatal")   return level::fatal;
    return level::null;
}

logger::logger()
    : m_messages(std::make_shared<messages_vector>()) {
}

logger::logger(const char *file, int line, const char *function)
    : m_context(file, line, function)
    , m_messages(std::make_shared<messages_vector>()) {
}

logger::logger(const settings &_settings)
    : m_messages(std::make_shared<messages_vector>())
    , m_settings(std::make_shared<settings>(_settings)) {
}

logger::logger(const settings &_settings, const char *file, int line, const char *function)
    : m_context(file, line, function)
    , m_messages(std::make_shared<messages_vector>())
    , m_settings(std::make_shared<settings>(_settings)) {
}

logger::logger(const std::string &category)
    : m_messages(std::make_shared<messages_vector>()) {
    m_context.category = category;
}

logger::logger(const std::string &category, const char *file, int line, const char *function)
    : m_context(file, line, function)
    , m_messages(std::make_shared<messages_vector>()) {
    m_context.category = category;
}

logger::logger(const std::string &category, const settings &_settings)
    : m_messages(std::make_shared<messages_vector>()) {
    m_context.category = category;
}

logger::logger(const std::string &category, const settings &_settings, const char *file, int line, const char *function)
    : m_context(file, line, function)
    , m_messages(std::make_shared<messages_vector>())
    , m_settings(std::make_shared<settings>(_settings)) {
    m_context.category = category;
}

const logger &logger::write(level _level, const char *msg, ...) const {
    va_list ap;
    va_start(ap, msg); // use variable arg list
    write(_level, msg, ap);
    va_end(ap);
    return *this;
}

const logger &logger::trace(const char *msg, ...) const {
    va_list ap;
    va_start(ap, msg); // use variable arg list
    write(level::trace, msg, ap);
    va_end(ap);
    return *this;
}

const logger &logger::debug(const char *msg, ...) const {
    va_list ap;
    va_start(ap, msg); // use variable arg list
    write(level::debug, msg, ap);
    va_end(ap);
    return *this;
}

const logger &logger::info(const char *msg, ...) const {
    va_list ap;
    va_start(ap, msg); // use variable arg list
    write(level::info, msg, ap);
    va_end(ap);
    return *this;
}

const logger &logger::warning(const char *msg, ...) const {
    va_list ap;
    va_start(ap, msg); // use variable arg list
    write(level::warning, msg, ap);
    va_end(ap);
    return *this;
}

const logger &logger::error(const char *msg, ...) const {
    va_list ap;
    va_start(ap, msg); // use variable arg list
    write(level::error, msg, ap);
    va_end(ap);
    return *this;
}

const logger &logger::fatal(const char *msg, ...) const {
    va_list ap;
    va_start(ap, msg); // use variable arg list
    write(level::fatal, msg, ap);
    va_end(ap);
    return *this;
}

const logger &logger::flush() const {
    if (m_messages->empty())
        return *this;

    bool has_fatal = false;
    for (auto &message : *m_messages) {
        if (!message.is_valid() || message.empty())
            continue;

        has_fatal = (message.level() == level::fatal);

        context ctx = m_context;
        message.update_context(ctx);

        // proceed with local settings if it's present
        if (m_settings && proceed_with_settings(*m_settings, message.level(), message.str(), m_context))
            continue;

        // proceed with global settings if it's present
        if (proceed_with_settings(s_settings, message.level(), message.str(), m_context))
            continue;

        // flush the message to the standard output
        message.flush_to_stdout(m_context.category);
    }

    // if we has atleast one fatal message abort the program
    if (has_fatal)
        std::abort();

    // clear messages
    m_messages->clear();
    return *this;
}

logger::settings &logger::get_settings() {
    return s_settings;
}

bool logger::check(bool cond, const char *msg, ...) const {
    if (cond)
        return true;

    va_list ap;
    va_start(ap, msg);
    write(pgr::logger::level::trace, msg, ap);
    va_end(ap);
    return false;
}

bool logger::check_debug(bool cond, const char *msg, ...) const {
    if (cond)
        return true;

    if (pgr::logger::is_level_enabled_globaly(pgr::logger::level::debug)) {
        va_list ap;
        va_start(ap, msg);
        write(pgr::logger::level::trace, msg, ap);
        va_end(ap);
    }
    return false;
}

bool logger::check_info(bool cond, const char *msg, ...) const {
    if (cond)
        return true;

    if (pgr::logger::is_level_enabled_globaly(pgr::logger::level::info)) {
        va_list ap;
        va_start(ap, msg);
        write(pgr::logger::level::info, msg, ap);
        va_end(ap);
    }
    return false;
}

bool logger::check_warning(bool cond, const char *msg, ...) const {
    if (cond)
        return true;

    if (pgr::logger::is_level_enabled_globaly(pgr::logger::level::warning)) {
        va_list ap;
        va_start(ap, msg);
        write(pgr::logger::level::warning, msg, ap);
        va_end(ap);
    }
    return false;
}

bool logger::check_error(bool cond, const char *msg, ...) const {
    if (cond)
        return true;

    if (pgr::logger::is_level_enabled_globaly(pgr::logger::level::debug)) {
        va_list ap;
        va_start(ap, msg);
        write(pgr::logger::level::error, msg, ap);
        va_end(ap);
    }
    return false;
}

void logger::write(level _level, const char *_msg, va_list ap) const {
    m_messages->emplace_back(_level, string_format(_msg, ap));
}

std::stringstream &logger::stream(level _level) const {
    m_messages->emplace_back(_level);
    return m_messages->back().stream();
}

bool logger::is_level_enabled_globaly(pgr::logger::level _level) {
    return is_level_enabled_helper(s_settings.default_level, _level);
}

bool logger::is_level_enabled_globaly(const std::string &_category, pgr::logger::level _level) {
    return is_level_enabled_helper(s_settings.get_category_level(_category), _level);
}

bool logger::is_level_enabled(pgr::logger::level _level) const {
    if (m_settings)
        return is_level_enabled_helper(m_settings->default_level, _level);
    return is_level_enabled_helper(s_settings.default_level, _level);
}

bool logger::is_level_enabled(const std::string &_category, pgr::logger::level _level) const {
    if (m_settings)
        return is_level_enabled_helper(m_settings->get_category_level(_category), _level);
    return is_level_enabled_helper(s_settings.get_category_level(_category), _level);
}

}  // namespace pgr
