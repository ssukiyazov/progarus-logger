/*
 * logger.h
 *
 *  Created on: Jul 30 2020
 *      Author: Sergey A. Sukiyazov <sergey.sukiyazov@gmail.com>
 *     License: The 3-Clause BSD License
 *
 *--------------------------------------------------------------------------------------------------
 * Copyright 2020 Sergey A. Sukiyazov
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted
 * provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions
 *    and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions
 *    and the following disclaimer in the documentation and/or other materials provided with the
 *    distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse
 *    or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *--------------------------------------------------------------------------------------------------
 */
#ifndef PROGARUS_LOGGER_H_
#define PROGARUS_LOGGER_H_

#include <string>
#include <sstream>
#include <thread>
#include <vector>
#include <map>
#include <functional>
#include <memory>
#include <cstdarg>
#include <chrono>
#include <ctime>

namespace pgr {

/**
 * @class logger
 * @brief This class represents the logger functionality for the C++ programs.
 *
 * This logger allows to change logging level and set callback which can redirect log output to any
 * place - file, network and etc.
 *
 * Example:
 * @code{.cpp}
 *  static void callback(pgr::logger::level level, const std::string &message, const pgr::logger::context &context) {
 *      // do actual log output here...
 *  }
 *
 *  int main(int argc, char **argv) {
 *      pgr::logger::get_settings().default_level = pgr::logger::level::all;
 *      pgr::logger::get_settings().callback = callback;
 *
 *      pgr::log().info() << "Staring...." << ", __cplusplus = " << __cplusplus;
 *      pgr::log().check(true, "Checking %d", 14566);
 *
 *      pgr::log().info() << "Done...";
 *      return 0;
 *  }
 * @endcode
 */
class logger {
public:
    /**
     * @brief Alias for clock type (the steady clock) which will use in the logger
     */
    using clock = std::chrono::steady_clock;

    /**
     * @fn std::chrono::system_clock::time_point steady_to_system(const clock::time_point&)
     * @brief Converts the std::chrono::steady_clock::time_point to std::chrono::system_clock::time_point.
     *
     * @param t std::chrono::steady_clock::time_point value.
     * @return std::chrono::system_clock::time_point value.
     */
    static std::chrono::system_clock::time_point steady_to_system(const clock::time_point &t) {
        return std::chrono::system_clock::now() + std::chrono::duration_cast<std::chrono::system_clock::duration>(t - clock::now());
    }

    /**
     * @fn std::time_t steady_to_unix_time(const clock::time_point&)
     * @brief  Converts the std::chrono::steady_clock::time_point to unix time.
     *
     * @param t std::chrono::steady_clock::time_point value.
     * @return Unix time value.
     */
    static std::time_t steady_to_unix_time(const clock::time_point &t) {
        return std::chrono::system_clock::to_time_t(steady_to_system(t));
    }


    /**
     * @class ios_flag_saver
     * @brief This class allows to keep and restore the settings and flags on IO streams.
     *
     */
    class ios_flag_saver {
    public:
        explicit ios_flag_saver(std::ostream &os) : m_os(os) { m_state.copyfmt(m_os); }
        ~ios_flag_saver() { m_os.copyfmt(m_state); }

        ios_flag_saver(const ios_flag_saver &rhs) = delete;
        ios_flag_saver &operator=(const ios_flag_saver &rhs) = delete;
    private:
        std::ostream &m_os;
        std::ios     m_state{nullptr};
    };

    /**
     * @struct context
     * @brief Represents the context of log message.
     */
    struct context {
        std::string       file;                                     //! The source code file name
        int               line = -1;                                //! The source code line number
        std::string       function;                                 //! The function/method name
        std::thread::id   thread_id = std::this_thread::get_id();   //! The thread id from which log came
        std::string       category;                                 //! The log category
        clock::time_point at = clock::now();                        //! The time at which log came

        context() = default;
        context(const context &) = default;
        context(const char *_file, int _line, const char *_function)
            : file(_file)
            , line(_line)
            , function(_function != nullptr ? _function : "")
            , thread_id(std::this_thread::get_id()) {
        }

        context &operator=(const context &) = default;

        /**
         * @fn bool is_null() const
         * @brief Checks if the context is null or not.
         *
         * @return @a @c true if the context is null (file, function, category are empty and line less
         *      than zero) and @a  @c false otherwise.
         */
        bool is_null() const { return file.empty() && line < 0 && function.empty() && category.empty(); }

        /**
         * @fn bool empty() const
         * @brief Checks if the context is empty or not.
         *
         * @return @a @c true if the context is null (file, function are empty and line less than zero)
         *      and @a  @c false otherwise.
         */
        bool empty() const { return file.empty() && line < 0 && function.empty(); }
    };

    /**
     * @enum level
     * @brief Represents logging level constants
     */
    enum class level : int {
        off = -102,     //! all log messages will be turned off
        all = -101,     //! all log messages will be turned on
        null = 0,       //! null
        trace = 1,      //! for trace messages (or trace and higher level messages will be on)
        debug,          //! for debug messages (or debug and higher level messages will be on)
        info,           //! for information messages (or information and higher level messages will be on)
        warning,        //! for warning messages (or warning and higher level messages will be on)
        error,          //! for error messages (or error and higher level messages will be on)
        fatal           //! for fatal messages (or and higher level messages will be on)
    };

    /**
     * @fn std::string level_to_name(level)
     * @brief Returns logging level name for specific logging level value.
     *
     * @param _level The logging level value.
     * @return The logging level name.
     */
    static std::string level_to_name(level _level);

    /**
     * @fn level name_to_level(const std::string&)
     * @brief Returns logging level value for specific logging level name.
     *
     * @param name The logging level name.
     * @return The logging level name or @sa level::null for invalid/wrong logging level name.
     */
    static level name_to_level(const std::string &name);


    /**
     * @brief Alias for logging callback function.
     */
    using callbac_func = std::function<void(level, const std::string &message, const context &ctx)>;

    /**
     * @class settings
     * @brief Represents logger settings.
     */
    class settings {
    public:
        logger::level default_level = logger::level::info;  //! Default logger level.
        callbac_func  callback;                             //! The logger callback.

        /**
         * @fn void set_category_level(const std::string&, pgr::logger::level)
         * @brief Sets the default logger level for specific category.
         * @param _category The category.
         * @param _level The default logger level.
         */
        void set_category_level(const std::string &_category, pgr::logger::level _level) {
            m_category_levels.emplace(_category, _level);
        }

        /**
         * @fn void remove_category_level(const std::string&)
         * @brief Removes the default logger level for specific category.
         * @param _category The category.
         */
        void remove_category_level(const std::string &_category) {
            m_category_levels.erase(_category);
        }

        /**
         * @fn logger::level get_category_level(const std::string&)
         * @brief Returns the default logger level for specific category.
         * @param _category The category.
         * @return The default logger level for requested category, or common default logger level
         *      if it isn't set for requested category.
         */
        logger::level get_category_level(const std::string &_category) {
            auto it = m_category_levels.find(_category);
            if (it == m_category_levels.end())
                return default_level;
            return it->second;
        }

    private:
        std::map<std::string,logger::level> m_category_levels; //! Holds the default logger levels per category.
    };

    // disable copy and assignment
    logger(const logger &) = default;
    logger &operator=(const logger &) = default;

    /**
     * @fn  logger()
     * @brief Constructs logger instance.
     */
    logger();

    /**
     * @fn  logger(const char*, int, const char*=nullptr)
     * @brief Constructs logger instance.
     *
     * @param file The source code file name.
     * @param line The source code line number.
     * @param function The function/method name.
     */
    logger(const char *file, int line, const char *function = nullptr);

    /**
     * @fn  logger(const settings&)
     * @brief Constructs logger instance.
     *
     * @param _settings The settings for the logger.
     */
    logger(const settings &_settings);

    /**
     * @fn  logger(const settings&, const char*, int, const char*=nullptr)
     * @brief Constructs logger instance.
     *
     * @param _settings The settings for the logger.
     * @param file The source code file name.
     * @param line The source code line number.
     * @param function The function/method name.
     */
    logger(const settings &_settings, const char *file, int line, const char *function = nullptr);

    /**
     * @fn  logger(const std::string&)
     * @brief Constructs logger instance.
     *
     * @param category The logging category.
     */
    logger(const std::string &category);

    /**
     * @fn  logger(const std::string &, const char *, int, const char *=nullptr);
     * @brief Constructs logger instance.
     *
     * @param category The logging category.
     * @param file The source code file name.
     * @param line The source code line number.
     * @param function The function/method name.
     */
    logger(const std::string &category, const char *file, int line, const char *function = nullptr);

    /**
     * @fn  logger(const std::string&, const settings &)
     * @brief Constructs logger instance.
     *
     * @param category The logging category.
     * @param _settings The settings for the logger.
     */
    logger(const std::string &category, const settings &_settings);

    /**
     * @fn  logger(const std::string&, const settings &, const char *, int, const char *=nullptr)
     * @brief Constructs logger instance.
     *
     * @param category The logging category.
     * @param _settings The settings for the logger.
     * @param file The source code file name.
     * @param line The source code line number.
     * @param function The function/method name.
     */
    logger(const std::string &category, const settings &_settings, const char *file, int line, const char *function = nullptr);

    /**
     * @fn  ~logger()
     * @brief Destroys logger instance.
     */
    ~logger() { flush(); }

    /**
     * @fn logger category(const std::string&, bool=true) const
     * @brief Return the logger instance with different category.
     *
     * @param category The logger category.
     * @param do_flush Is the flush of streams for this instance of logger required or not.
     * @return The logger instance with different category.
     */
    logger category(const std::string &category, bool do_flush = true) const {
        if (do_flush)
            flush();

        logger _logger;
        _logger.m_settings = m_settings;
        _logger.m_context = m_context;;
        _logger.m_context.category = category;
        return _logger;
    }

    /**
     * @fn std::string category() const
     * @brief Returns the this logger category.
     *
     * @return This logger category.
     */
    std::string category() const { return m_context.category; }

    /**
     * @fn const logger &write(level, const char*, ...) const
     * @brief Writes message to the logger.
     *
     * @param _level Log level for the message.
     * @param msg The message (the @sa printf(...) formating is allowed)
     * @return This instance of logger.
     */
    const logger &write(level _level, const char *msg, ...) const;

    /**
     * @fn const logger &trace(const char*, ...) const
     * @brief Writes trace level message to the logger.
     *
     * @param msg The message (the @sa printf(...) formating is allowed)
     * @return This instance of logger.
     */
    const logger &trace(const char *msg, ...) const;

    /**
     * @fn const logger &debug(const char*, ...) const
     * @brief Writes debug level message to the logger.
     *
     * @param msg The message (the @sa printf(...) formating is allowed)
     * @return This instance of logger.
     */
    const logger &debug(const char *msg, ...) const;

    /**
     * @fn const logger &info(const char*, ...) const
     * @brief Writes info level message to the logger.
     *
     * @param msg The message (the @sa printf(...) formating is allowed)
     * @return This instance of logger.
     */
    const logger &info(const char *msg, ...) const;

    /**
     * @fn const logger &warning(const char*, ...) const
     * @brief Writes warning level message to the logger.
     *
     * @param msg The message (the @sa printf(...) formating is allowed)
     * @return This instance of logger.
     */
    const logger &warning(const char *msg, ...) const;

    /**
     * @fn const logger &error(const char*, ...) const
     * @brief Writes error level message to the logger.
     *
     * @param msg The message (the @sa printf(...) formating is allowed)
     * @return This instance of logger.
     */
    const logger &error(const char *msg, ...) const;

    /**
     * @fn const logger &fatal(const char*, ...) const
     * @brief Writes fatal level message to the logger.
     *
     * @param msg The message (the @sa printf(...) formating is allowed)
     * @return This instance of logger.
     */
    const logger &fatal(const char *msg, ...) const;

    /**
     * @fn std::stringstream &stream(level) const
     * @brief Returns the std::stringstream which is associated with the logger for specific log level.
     *
     * @param _level Log level for the message.
     * @return The std::stringstream which is associated with the logger for specific log level.
     */
    std::stringstream &stream(level _level) const;

    /**
     * @fn std::stringstream &trace() const
     * @brief Returns the std::stringstream which is associated with the logger for trace log level.
     *
     * @return The std::stringstream which is associated with the logger for trace log level.
     */
    std::stringstream &trace() const { return stream(level::trace); }

    /**
     * @fn std::stringstream &debug() const
     * @brief Returns the std::stringstream which is associated with the logger for debug log level.
     *
     * @return The std::stringstream which is associated with the logger for debug log level.
     */
    std::stringstream &debug() const { return stream(level::debug); }

    /**
     * @fn std::stringstream &info() const
     * @brief Returns the std::stringstream which is associated with the logger for info log level.
     *
     * @return The std::stringstream which is associated with the logger for info log level.
     */
    std::stringstream &info() const { return stream(level::info); }

    /**
     * @fn std::stringstream &warning() const
     * @brief Returns the std::stringstream which is associated with the logger for warning log level.
     *
     * @return The std::stringstream which is associated with the logger for warning log level.
     */
    std::stringstream &warning() const { return stream(level::warning); }

    /**
     * @fn std::stringstream &error() const
     * @brief Returns the std::stringstream which is associated with the logger for error log level.
     *
     * @return The std::stringstream which is associated with the logger for error log level.
     */
    std::stringstream &error() const { return stream(level::error); }

    /**
     * @fn std::stringstream &fatal() const
     * @brief Returns the std::stringstream which is associated with the logger for fatal log level.
     *
     * @return The std::stringstream which is associated with the logger for fatal log level.
     */
    std::stringstream &fatal() const { return stream(level::fatal); }

    /**
     * @fn const logger &flush() const
     * @brief Flushes the contents of logger.
     *
     * @return This instance of logger.
     */
    const logger &flush() const;

    //TODO: produces an error on attempt to overload with static method
    // settings &get_settings() { return m_settings; }
    // settings get_settings() const { return m_settings; }

    /**
     * @fn settings &get_settings()
     * @brief Returns settings for the this instance of logger.
     *
     * @return Settings for the this instance of logger.
     */
    static settings &get_settings();

    /**
     * @fn bool is_level_enabled_globaly(pgr::logger::level)
     * @brief Check if the requested logging level is enabled by global settings.
     *
     * @param _level The requested logging level.
     * @return @a @c true if the level is enabled and @a @c false otherwise.
     */
    static bool is_level_enabled_globaly(pgr::logger::level _level);

    /**
     * @fn bool is_level_enabled_globaly(const std::string&, pgr::logger::level)
     * @brief Check if the requested logging level is enabled by global settings for specific category.
     *
     * @param _category The category to check.
     * @param _level The requested logging level.
     * @return @a @c true if the level is enabled and @a @c false otherwise.
     */
    static bool is_level_enabled_globaly(const std::string &_category, pgr::logger::level _level);

    /**
     * @fn bool is_level_enabled(pgr::logger::level) const
     * @brief Check if the requested logging level is enabled by logger settings.
     *
     * @param _level The requested logging level.
     * @return @a @c true if the level is enabled and @a @c false otherwise.
     */
    bool is_level_enabled(pgr::logger::level _level) const;

    /**
     * @fn bool is_level_enabled(const std::string&, pgr::logger::level)const
     * @brief Check if the requested logging level is enabled by logger settings for specific category.
     *
     * @param _category The category to check.
     * @param _level The requested logging level.
     * @return @a @c true if the level is enabled and @a @c false otherwise.
     */
    bool is_level_enabled(const std::string &_category, pgr::logger::level _level) const;

    /**
     * @fn bool check(bool, const char*, ...) const
     * @brief Checks the condition and prints the trace message into log if condition is @a @c false.
     *
     * @param cond The condition.
     * @param msg The message (the @sa printf(...) formating is allowed)
     * @return The condition value.
     */
    bool check(bool cond, const char *msg, ...) const;

    /**
     * @fn bool check_debug(bool, const char*, ...) const
     * @brief Checks the condition and prints the debug message into log if condition is @a @c false.
     *
     * @param cond The condition.
     * @param msg The message (the @sa printf(...) formating is allowed)
     * @return The condition value.
     */
    bool check_debug(bool cond, const char *msg, ...) const;

    /**
     * @fn bool check_info(bool, const char*, ...) const
     * @brief Checks the condition and prints the info message into log if condition is @a @c false.
     *
     * @param cond The condition.
     * @param msg The message (the @sa printf(...) formating is allowed)
     * @return The condition value.
     */
    bool check_info(bool cond, const char *msg, ...) const;

    /**
     * @fn bool check_warning(bool, const char*, ...) const
     * @brief Checks the condition and prints the warning message into log if condition is @a @c false.
     *
     * @param cond The condition.
     * @param msg The message (the @sa printf(...) formating is allowed)
     * @return The condition value.
     */
    bool check_warning(bool cond, const char *msg, ...) const;

    /**
     * @fn bool check_error(bool, const char*, ...) const
     * @brief Checks the condition and prints the error message into log if condition is @a @c false.
     *
     * @param cond The condition.
     * @param msg The message (the @sa printf(...) formating is allowed)
     * @return The condition value.
     */
    bool check_error(bool cond, const char *msg, ...) const;

protected:
    /**
     * @fn void write(level, const char*, va_list)const
     * @brief Writes message to the logger.
     *
     * @param _level Log level for the message.
     * @param _msg The message (the @sa printf(...) formating is allowed)
     * @param ap The variadic parameters pointer.
     */
    void write(level _level, const char *_msg, va_list ap) const;

private:
    class message;
    using messages_vector = std::vector<message>;

    context                                  m_context;
    mutable std::shared_ptr<messages_vector> m_messages;
    std::shared_ptr<settings>                m_settings;
};


/**
 * @namespace log_helpers
 * @brief Various helpers for logger output.
 */
namespace log_helpers {

/**
 * @fn std::string yes_no(bool)
 * @brief Prints a bool value as "yes/no" form.
 * @param value The value to print.
 * @return A string with "yes" for true value and "no" otherwise.
 */
inline std::string yes_no(bool value) {
    return std::move(std::string(value ? "yes" : "no"));
}

/**
 * @fn std::string true_false(bool)
 * @brief Prints a bool value as "true/false" form.
 * @param value The value to print.
 * @return A string with "true" for true value and "false" otherwise.
 */
inline std::string true_false(bool value) {
    return std::move(std::string(value ? "true" : "false"));
}

/**
 * @fn std::string on_off(bool)
 * @brief Prints a bool value as "on/off" form.
 * @param value The value to print.
 * @return A string with "on" for true value and "off" otherwise.
 */
inline std::string on_off(bool value) {
    return std::move(std::string(value ? "on" : "off"));
}

}  // namespace log_helpers
}  // namespace pgr

#define __LOGGER_FILE__ static_cast<const char *>(__FILE__)
#define __LOGGER_LINE__ __LINE__
#if defined(__linux__)
# if defined(__GNUC__)
#  define __LOGGER_FUNCTION__ static_cast<const char *>(__PRETTY_FUNCTION__)
# endif // defined(__GNUC__)
#elif defined(WIN32)
# if defined(_MSC_VER)
#  define __LOGGER_FUNCTION__ static_cast<const char *>(__FUNCSIG__)
# endif // defined(_MSC_VER)
#endif
#if !defined(__LOGGER_FUNCTION__)
#  define __LOGGER_FUNCTION__ static_cast<const char *>(__FUNCTION__)
#endif // !defined(__LOGGER_FUNCTION__)

/**
 * @def def_logger
 * @brief Macros which allows to pass current source code filename, line number and etc. to the constructor
 *      of logger instance.
 *
 * Example:
 * @code{.cpp}
 *  pgr::def_logger().trace() << "--> It's a trace message";
 *  pgr::def_logger().debug() << "--> It's a debug message";
 *  pgr::def_logger().info() << "--> It's a information message";
 *  pgr::def_logger().warning() << "--> It's a warning message";
 *  pgr::def_logger().error() << "--> It's a error message";
 *
 *  pgr::def_logger().trace("--> It's a prinf-like %s message", "trace");
 *  pgr::def_logger().debug("--> It's a prinf-like %s message", "debug");
 *  pgr::def_logger().info("--> It's a prinf-like %s message", "info");
 *  pgr::def_logger().warning("--> It's a prinf-like %s message", "warning");
 *  pgr::def_logger().error("--> It's a prinf-like %s message", "error");
 *
 *  pgr::def_logger().check(true, "Checking for %s condition", "true");
 *  pgr::def_logger().check(false, "Checking for %s condition", "false");
 * @endcode
 */
#define def_logger() logger(__LOGGER_FILE__, __LOGGER_LINE__, __LOGGER_FUNCTION__)

/**
 * @def log_c
 * @brief Macros which allows to pass the specific category, current source code filename, line number
 *      and etc. to the constructor of logger instance.
 *
 * Example:
 * @code{.cpp}
 *  const std::string category = "category_1";
 *
 *  pgr::log_c(category).trace() << "--> It's a trace message";
 *  pgr::log_c(category).debug() << "--> It's a debug message";
 *  pgr::log_c(category).info() << "--> It's a information message";
 *  pgr::log_c(category).warning() << "--> It's a warning message";
 *  pgr::log_c(category).error() << "--> It's a error message";
 *
 *  pgr::log_c(category).trace("--> It's a prinf-like %s message", "trace");
 *  pgr::log_c(category).debug("--> It's a prinf-like %s message", "debug");
 *  pgr::log_c(category).info("--> It's a prinf-like %s message", "info");
 *  pgr::log_c(category).warning("--> It's a prinf-like %s message", "warning");
 *  pgr::log_c(category).error("--> It's a prinf-like %s message", "error");
 *
 *  pgr::log_c(category).check(true, "Checking for %s condition", "true");
 *  pgr::log_c(category).check(false, "Checking for %s condition", "false");
 * @endcode
 */
#define log_c(c) logger((c), __LOGGER_FILE__, __LOGGER_LINE__, __LOGGER_FUNCTION__)

/**
 * @def log_trace
 * @brief Short form of log macro.
  */
#define log_trace(...) \
    if (pgr::logger::is_level_enabled_globaly(pgr::logger::level::trace)) \
        pgr::logger(__LOGGER_FILE__, __LOGGER_LINE__, __LOGGER_FUNCTION__).trace(__VA_ARGS__)

/**
 * @def log_debug
 * @brief Short form of log macro.
  */
#define log_debug(...) \
    if (pgr::logger::is_level_enabled_globaly(pgr::logger::level::debug)) \
        pgr::logger(__LOGGER_FILE__, __LOGGER_LINE__, __LOGGER_FUNCTION__).debug(__VA_ARGS__)

/**
 * @def log_info
 * @brief Short form of log macro.
  */
#define log_info(...) \
    if (pgr::logger::is_level_enabled_globaly(pgr::logger::level::info)) \
        pgr::logger(__LOGGER_FILE__, __LOGGER_LINE__, __LOGGER_FUNCTION__).info(__VA_ARGS__)

/**
 * @def log_warning
 * @brief Short form of log macro.
  */
#define log_warning(...) \
    if (pgr::logger::is_level_enabled_globaly(pgr::logger::level::warning)) \
        pgr::logger(__LOGGER_FILE__, __LOGGER_LINE__, __LOGGER_FUNCTION__).warning(__VA_ARGS__)

/**
 * @def log_error
 * @brief Short form of log macro.
  */
#define log_error(...) \
    if (pgr::logger::is_level_enabled_globaly(pgr::logger::level::error)) \
        pgr::logger(__LOGGER_FILE__, __LOGGER_LINE__, __LOGGER_FUNCTION__).error(__VA_ARGS__)

/**
 * @def log_fatal
 * @brief Short form of log macro.
  */
#define log_fatal(...) \
    if (pgr::logger::is_level_enabled_globaly(pgr::logger::level::fatal)) \
        pgr::logger(__LOGGER_FILE__, __LOGGER_LINE__, __LOGGER_FUNCTION__).fatal(__VA_ARGS__)

/**
 * @def log_trace_c
 * @brief Short form of log macro with a category.
 */
#define log_trace_c(category, ...) \
    if (pgr::logger::is_level_enabled_globaly((category), pgr::logger::level::trace)) \
        pgr::logger((category), __LOGGER_FILE__, __LOGGER_LINE__, __LOGGER_FUNCTION__).trace(__VA_ARGS__)

/**
 * @def log_debug_c
 * @brief Short form of log macro with a category.
  */
#define log_debug_c(category, ...) \
    if (pgr::logger::is_level_enabled_globaly((category), pgr::logger::level::debug)) \
        pgr::logger((category), __LOGGER_FILE__, __LOGGER_LINE__, __LOGGER_FUNCTION__).debug(__VA_ARGS__)

/**
 * @def log_info_c
 * @brief Short form of log macro with a category.
  */
#define log_info_c(category, ...) \
    if (pgr::logger::is_level_enabled_globaly((category), pgr::logger::level::info)) \
        pgr::logger((category), __LOGGER_FILE__, __LOGGER_LINE__, __LOGGER_FUNCTION__).info(__VA_ARGS__)

/**
 * @def log_warning_c
 * @brief Short form of log macro a category.
  */
#define log_warning_c(category, ...) \
    if (pgr::logger::is_level_enabled_globaly((category), pgr::logger::level::warning)) \
        pgr::logger((category), __LOGGER_FILE__, __LOGGER_LINE__, __LOGGER_FUNCTION__).warning(__VA_ARGS__)

/**
 * @def log_error_c
 * @brief Short form of log macro a category.
  */
#define log_error_c(category, ...) \
    if (pgr::logger::is_level_enabled_globaly((category), pgr::logger::level::error)) \
        pgr::logger((category), __LOGGER_FILE__, __LOGGER_LINE__, __LOGGER_FUNCTION__).error(__VA_ARGS__)

/**
 * @def log_fatal_c
 * @brief Short form of log macro a category.
  */
#define log_fatal_c(category, ...) \
    if (pgr::logger::is_level_enabled_globaly((category), pgr::logger::level::fatal)) \
        pgr::logger((category), __LOGGER_FILE__, __LOGGER_LINE__, __LOGGER_FUNCTION__).fatal(__VA_ARGS__)

/**
 * @def log_check
 * @brief Checks the condition and prints the trace message into log if condition is @a @c false.
 */
#define log_check(...) \
    (pgr::logger(__LOGGER_FILE__, __LOGGER_LINE__, __LOGGER_FUNCTION__).check(__VA_ARGS__))

/**
 * @def log_check_debug
 * @brief Checks the condition and prints the trace message into log if condition is @a @c false.
 */
#define log_check_debug(...) \
    (pgr::logger(__LOGGER_FILE__, __LOGGER_LINE__, __LOGGER_FUNCTION__).check_debug(__VA_ARGS__))

/**
 * @def log_check_info
 * @brief Checks the condition and prints the trace message into log if condition is @a @c false.
 */
#define log_check_info(...) \
    (pgr::logger(__LOGGER_FILE__, __LOGGER_LINE__, __LOGGER_FUNCTION__).check_info(__VA_ARGS__))

/**
 * @def log_check_warning
 * @brief Checks the condition and prints the trace message into log if condition is @a @c false.
 */
#define log_check_warning(...) \
    (pgr::logger(__LOGGER_FILE__, __LOGGER_LINE__, __LOGGER_FUNCTION__).check_warning(__VA_ARGS__))

/**
 * @def log_check_error
 * @brief Checks the condition and prints the trace message into log if condition is @a @c false.
 */
#define log_check_error(...) \
    (pgr::logger(__LOGGER_FILE__, __LOGGER_LINE__, __LOGGER_FUNCTION__).check_error(__VA_ARGS__))


#endif /* PROGARUS_LOGGER_H_ */
