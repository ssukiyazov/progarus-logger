This class represents the logger functionality for the C++ programs.
=====================================================

This logger allows to change logging level and set callback which can redirect log output to any  place - file, network and etc.

Example of ussage. The directory exampleapp contains a simple sample of usage with simple code of two log-appenders: console and file with file rotation.


You just need to copy "logger/logger.h" and "logger/logger.cpp" into ypur project an then use the code similar below:

	#include "logger/logger.h"
	
	// The application logger class - it should implement all functionlaity which you need for logging in your application (write to file, send through network and etc) in the void operator()(pgr::logger::level level, const std::string &message, const pgr::logger::context &context).
	class app_logger {
	public:
	    app_logger() = delete;
	    app_logger &operator=(const app_logger &) = delete;
	
	    app_logger(int argc, char **argv) {
	        m_preffered_level = pgr::logger::level::all;
	    }
	
	    bool is_valid() const { return m_is_valid; }
	    pgr::logger::level preffered_level() const { return m_preffered_level; }
	
	    void operator()(pgr::logger::level level, const std::string &message, const pgr::logger::context &context) {
	        std::stringstream ss;
	
	        if (m_print_time_point) {
	            std::time_t t_c = pgr::logger::steady_to_unix_time(context.at);
	            if (m_time_point_gmt)
	                ss << std::put_time(std::gmtime(&t_c), "%c %Z");
	            else
	                ss << std::put_time(std::localtime(&t_c), "%c %Z");
	            ss << "| ";
	        }
	
	        ss << "exampleapp| ";
	        if (m_print_thread_id)
	            ss << context.thread_id << "| ";
	
	        ss << pgr::logger::level_to_name(level);
	
	        if (!context.category.empty())
	            ss << "." << context.category;
	
	        ss << ": " << message;
	
	        if (!context.is_null() && m_print_context) {  // Format information about place in the code. turned off now
	            ss << ", file = " << context.file;
	            if (context.line >= 0)
	                ss << ", line = " << context.line;
	            if (!context.function.empty())
	                ss << ", function = " << context.function;
	        }
	
	        ss << std::endl;
	
	        { //NOTE: critical section here
	            std::lock_guard<std::mutex> lock(m_mutex);
	            std::ostream &os = (level > pgr::logger::level::info ? std::cerr : std::cout);
	            os << ss.str();
	            os.flush();
	        }
	    }
	
	private:
	    pgr::logger::level  m_preffered_level = pgr::logger::level::info;
	    std::mutex          m_mutex;
	    std::atomic_bool    m_is_valid = { true };
	    std::atomic_bool    m_print_context = { false };
	    std::atomic_bool    m_print_thread_id = { false };
	    std::atomic_bool    m_print_time_point = { true };
	    std::atomic_bool    m_time_point_gmt = { true };
	
	};
	
	void print_usage() {
		// print usage message here
	}
	
	int main(int argc, char **argv) {
	    std::setlocale(LC_ALL, "");
	
	    app_logger _logger(argc, argv);
	    if (!_logger.is_valid()) {
	        print_usage();
	        return 255;
	    }
	
	    pgr::logger::get_settings().default_level = _logger.preffered_level();
	    pgr::logger::get_settings().callback = std::ref(_logger);
	
	    pgr::log().info() << "Staring...." << ", __cplusplus = " << __cplusplus;
	
	    // regular log messages
	    pgr::log().trace() << "--> It's a trace message";
	    pgr::log().debug() << "--> It's a debug message";
	    pgr::log().info() << "--> It's a information message";
	    pgr::log().warning() << "--> It's a warning message";
	    pgr::log().error() << "--> It's a error message";
	
	    pgr::log().trace("--> It's a prinf-like %s message", "trace");
	    pgr::log().debug("--> It's a prinf-like %s message", "debug");
	    pgr::log().info("--> It's a prinf-like %s message", "info");
	    pgr::log().warning("--> It's a prinf-like %s message", "warning");
	    pgr::log().error("--> It's a prinf-like %s message", "error");
	
	    pgr::log().check(true, "Checking for %s condition", "true");
	    pgr::log().check(false, "Checking for %s condition", "false");
	
	    // categorized log messages
	    const std::string category = "category_1";
	    pgr::log_c(category).trace() << "--> It's a trace message";
	    pgr::log_c(category).debug() << "--> It's a debug message";
	    pgr::log_c(category).info() << "--> It's a information message";
	    pgr::log_c(category).warning() << "--> It's a warning message";
	    pgr::log_c(category).error() << "--> It's a error message";
	
	    pgr::log_c(category).trace("--> It's a prinf-like %s message", "trace");
	    pgr::log_c(category).debug("--> It's a prinf-like %s message", "debug");
	    pgr::log_c(category).info("--> It's a prinf-like %s message", "info");
	    pgr::log_c(category).warning("--> It's a prinf-like %s message", "warning");
	    pgr::log_c(category).error("--> It's a prinf-like %s message", "error");
	
	    pgr::log_c(category).check(true, "Checking for %s condition", "true");
	    pgr::log_c(category).check(false, "Checking for %s condition", "false");
	
	    pgr::log().info() << "Done...";
	    return 0;
	}


