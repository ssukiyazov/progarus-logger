/*
 * appenders.h
 *
 *  Created on: 30 июн. 2021 г.
 *      Author: Sergey A. Sukiyazov <sergey.sukiyazov@gmail.com>
 */

#ifndef EXAMPLEAPP_APPENDERS_H_
#define EXAMPLEAPP_APPENDERS_H_

#include "logger/logger.h"

#include <string>
#include <iostream>
#include <fstream>
#include <mutex>
#include <atomic>
#include <filesystem>
#include <memory>
#include <iomanip>

namespace fs = std::filesystem;

class file_appender {
public:
    file_appender() = delete;
    file_appender &operator=(const file_appender &) = delete;

    file_appender(const std::string &name, int argc, char **argv) : m_name(name) {}

    std::string name() const { return m_name; }
    void set_name(const std::string &name) { m_name = name; }

    bool enabled() const { return m_enabled; }
    void set_enabled(bool enabled) { m_enabled = enabled; }

    std::uintmax_t  max_file_size() const { return m_max_file_size; }
    void set_max_file_size(std::uintmax_t max_file_size) { m_max_file_size = max_file_size; }

    size_t max_files() const { return m_max_files; }
    void set_max_files(size_t max_files) {m_max_files = max_files; }

    void operator()(pgr::logger::level level, const std::string &message, const pgr::logger::context &context) {
        if (!m_enabled)
            return;

        if (!fs::exists(m_path))
            if (!fs::create_directories(m_path))
                return;

        auto out_message =  format(level, message, context);
        if (!rotate(out_message.length()))
            return;

        { //NOTE: critical section here
            std::lock_guard<std::mutex> lock(m_mutex);

            if (!m_os)
                return;
            (*m_os) << out_message;
            m_os->flush();
        }
    }

protected:
    bool remove(const fs::path &path) {
        if (!fs::exists(path))
            return true;
        return fs::remove(path);
    }

    bool rename(const fs::path &from, const fs::path &to) {
        if (!remove(to))
            return false;
        if (!fs::exists(from))
            return true;
        fs::rename(from, to);
        return true;
    }

    bool rotate(size_t message_size) {
        std::string file_name = m_name + ".log";
        fs::path file_path = m_path / file_name;

        bool exists = fs::exists(file_path);
        std::uintmax_t file_size = message_size;
        if (exists)
            file_size += fs::file_size(file_path);

        if (file_size < m_max_file_size || m_max_files == 0) {
            if (m_os)
                return true;
            m_os = std::make_unique<std::ofstream>(file_path.string(), std::ios::app);
            if (!exists)
                 (*m_os) << "-=< Created >=-" << std::endl;
            return true;
        }

        if (m_os) {
            m_os->close();
            m_os.release();
        }

        std::string file_names[2];
        for (auto i = m_max_files; i > 0; i--){
            file_names[0] = m_name + ".log";
            file_names[1] = m_name + ".log_" + std::to_string(i);

            if (i > 1) {
                file_names[0].append("_");
                file_names[0].append(std::to_string(i-1));
            }

            if (!rename(m_path / file_names[0], m_path / file_names[1]))
                return false;
        }

        m_os = std::make_unique<std::ofstream>(file_path.string(), std::ios::app);
        (*m_os) << "-=< Created >=-" << std::endl;
        return true;
    }

    std::string format(pgr::logger::level level, const std::string &message, const pgr::logger::context &context) {
        const std::string delimitter = "\t";
        const char *time_format = "%F %T";
        std::stringstream ss;

        // calulate and log record timestamp
        auto tp = pgr::logger::steady_to_system(context.at);
        auto ms = std::chrono::duration_cast<std::chrono::milliseconds>(tp.time_since_epoch()) % 1000;
        
        std::time_t t_c = std::chrono::system_clock::to_time_t(tp);
        auto time_tm = (m_use_gmt_time ? std::gmtime(&t_c) : std::localtime(&t_c));

        ss << std::put_time(time_tm, time_format) << '.' << std::setfill('0') << std::setw(3) << ms.count();
        if (m_use_gmt_time)
            ss << " UTC";
        else
            ss << std::put_time(time_tm, " %z");
        ss << delimitter;

        if (m_out_thread_id)
            ss << context.thread_id << delimitter;

        ss << pgr::logger::level_to_name(level) << delimitter;

        if (!context.category.empty())
            ss << context.category;
        else
            ss << "default";

        ss << delimitter << message << delimitter;

        if (!context.is_null()) {  // Format information about place in the code. turned off now
            ss << ", file = " << context.file;
            if (context.line >= 0)
                ss << ", line = " << context.line;
            if (!context.function.empty())
                ss << ", function = " << context.function;
        }

        ss << std::endl;
        return ss.str();
    }

private:
    std::mutex          m_mutex;
    std::string         m_name = "file_appender";
    fs::path            m_path = fs::current_path();
    std::uintmax_t      m_max_file_size = 10 * 1024;
    size_t              m_max_files = 5;
    std::atomic_bool    m_enabled = { true },
                        m_use_gmt_time = { true },
                        m_out_thread_id = { false };

    std::unique_ptr<std::ofstream> m_os;
};

//--------------------------------------------------------------------------------------------------

class console_appender {
public:
    console_appender() = delete;
    console_appender &operator=(const console_appender &) = delete;

    console_appender(const std::string &name, int argc, char **argv) : m_name(name) {}

    std::string name() const { return m_name; }
    void set_name(const std::string &name) { m_name = name; }

    bool enabled() const { return m_enabled; }
    void set_enabled(bool enabled) { m_enabled = enabled; }

    void operator()(pgr::logger::level level, const std::string &message, const pgr::logger::context &context) {
        if (!m_enabled)
            return;

        std::stringstream ss;

        ss << m_name << "| ";
        if (m_out_thread_id)
            ss << context.thread_id << "| ";

        ss << pgr::logger::level_to_name(level);

        if (!context.category.empty())
            ss << "." << context.category;

        ss << ": " << message << std::endl;

        { //NOTE: critical section here
            std::lock_guard<std::mutex> lock(m_mutex);
            std::ostream &os = (level > pgr::logger::level::info ? std::cerr : std::cout);
            os << ss.str();
            os.flush();
        }
    }

private:
    std::mutex          m_mutex;
    std::string         m_name;
    std::atomic_bool    m_enabled = { true },
                        m_out_thread_id = { false };
};

#endif /* EXAMPLEAPP_APPENDERS_H_ */

