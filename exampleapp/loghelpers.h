/*
 * loghelpers.h
 *
 *  Created on: 30 июн. 2021 г.
 *      Author: Sergey A. Sukiyazov <sergey.sukiyazov@gmail.com>
 */

#ifndef EXAMPLEAPP_LOGHELPERS_H_
#define EXAMPLEAPP_LOGHELPERS_H_

#include <ostream>
#include <deque>
#include <forward_list>
#include <list>
#include <map>
#include <queue>
#include <set>
#include <stack>
#include <string>
#include <tuple>
#include <type_traits>
#include <unordered_map>
#include <unordered_set>
#include <utility>
#include <vector>
#include <type_traits>
#include <typeinfo>

//specialize a type for all of the STL containers.
namespace pgr {

// define type traits for sequence containers
template<typename _Type>    struct is_stl_seq_container                                      : std::false_type{};
template<typename _Type, std::size_t _N> struct is_stl_seq_container<std::array  <_Type,_N>> : std::true_type{};
template<typename... _Args> struct is_stl_seq_container<std::vector              <_Args...>> : std::true_type{};
template<typename... _Args> struct is_stl_seq_container<std::set                 <_Args...>> : std::true_type{};
template<typename... _Args> struct is_stl_seq_container<std::multiset            <_Args...>> : std::true_type{};
template<typename... _Args> struct is_stl_seq_container<std::deque               <_Args...>> : std::true_type{};
template<typename... _Args> struct is_stl_seq_container<std::list                <_Args...>> : std::true_type{};
template<typename... _Args> struct is_stl_seq_container<std::forward_list        <_Args...>> : std::true_type{};
template<typename... _Args> struct is_stl_seq_container<std::stack               <_Args...>> : std::true_type{};
template<typename... _Args> struct is_stl_seq_container<std::queue               <_Args...>> : std::true_type{};
template<typename... _Args> struct is_stl_seq_container<std::priority_queue      <_Args...>> : std::true_type{};
template<typename... _Args> struct is_stl_seq_container<std::unordered_set       <_Args...>> : std::true_type{};
template<typename... _Args> struct is_stl_seq_container<std::unordered_multiset  <_Args...>> : std::true_type{};

// define type traits for associative containers
template<typename _Type>    struct is_stl_assoc_container                                    : std::false_type{};
template<typename... _Args> struct is_stl_assoc_container<std::map               <_Args...>> : std::true_type{};
template<typename... _Args> struct is_stl_assoc_container<std::multimap          <_Args...>> : std::true_type{};
template<typename... _Args> struct is_stl_assoc_container<std::unordered_map     <_Args...>> : std::true_type{};
template<typename... _Args> struct is_stl_assoc_container<std::unordered_multimap<_Args...>> : std::true_type{};
}

//type trait to utilize the implementation type traits as well as decay the type
template <typename _Type> struct is_stl_seq_container {
    static constexpr bool const value = pgr::is_stl_seq_container<std::decay_t<_Type>>::value;
};

template <typename _Type> struct is_stl_assoc_container {
    static constexpr bool const value = pgr::is_stl_assoc_container<std::decay_t<_Type>>::value;
};

// Redefine ostream output operators for containers
//  - out value for sequence containers (use quotes for string values)
//  - out key-values pairs for associative containers (use quotes for string values)
template <typename _Type, std::enable_if_t<is_stl_seq_container<_Type>::value, bool> = true>
    inline std::ostream &operator<<(std::ostream &os, const _Type &values) {
        const char *quotes = (typeid(std::string) == typeid(typename _Type::value_type)) ? "\"" : "";

        os << "(";
        for (auto i = values.begin(); i != values.end(); ++i) {
            if (i != values.begin())
                os << ", ";
            os << quotes << *i << quotes;
        }
        os << ")";
        return os;
    }

template <typename _Type, std::enable_if_t<is_stl_assoc_container<_Type>::value, bool> = true>
    inline std::ostream &operator<<(std::ostream &os, const _Type &values) {
        const char *k_quotes = (typeid(std::string) == typeid(typename _Type::key_type)) ? "\"" : "";
        const char *v_quotes = (typeid(std::string) == typeid(typename _Type::value_type)) ? "\"" : "";

        os << "(";
        for (auto i = values.begin(); i != values.end(); ++i) {
            if (i != values.begin())
                os << ", ";
            os << "{" << k_quotes << i->first << k_quotes << " : " << v_quotes << i->second << v_quotes << "}";
        }
        os << ")";
        return os;
    }

#endif /* EXAMPLEAPP_LOGHELPERS_H_ */
