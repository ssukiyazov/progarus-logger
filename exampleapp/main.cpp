/*
 * main.cpp
 *
 *  Created on: Jul 30 2020
 *      Author: Sergey A. Sukiyazov <sergey.sukiyazov@gmail.com>
 *     License: The 3-Clause BSD License
 *
 *--------------------------------------------------------------------------------------------------
 * Copyright 2020 Sergey A. Sukiyazov
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted
 * provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions
 *    and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions
 *    and the following disclaimer in the documentation and/or other materials provided with the
 *    distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse
 *    or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *--------------------------------------------------------------------------------------------------
 */

#include "loghelpers.h"
#include "appenders.h"
#include "logger/logger.h"

#include <cstdint>
#include <iostream>
#include <string>
#include <clocale>
#include <mutex>
#include <atomic>
#include <cmath>

//--------------------------------------------------------------------------------------------------

namespace math {
// const double pi = 3.14159265358979311600;
inline double pi() { return std::atan(1)*4; }
}  // namespace math

//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------

class app_logger {
public:
    const std::string name = "exampleapp";

    app_logger() = delete;
    app_logger &operator=(const app_logger &) = delete;

    app_logger(int argc, char **argv)
        : m_cons_appender(name, argc, argv)
        , m_file_appender(name, argc, argv) {
        m_preffered_level = pgr::logger::level::all;
    }

    bool is_valid() const { return m_is_valid; }
    pgr::logger::level preffered_level() const { return m_preffered_level; }

    void operator()(pgr::logger::level level, const std::string &message, const pgr::logger::context &context) {
        m_cons_appender(level, message, context);
        m_file_appender(level, message, context);
    }

    console_appender &console() { return m_cons_appender; }
    file_appender &file() { return m_file_appender; }

private:
    pgr::logger::level  m_preffered_level = pgr::logger::level::info;
    std::mutex          m_mutex;
    std::atomic_bool    m_is_valid = { true };

    console_appender    m_cons_appender;
    file_appender       m_file_appender;
};

//--------------------------------------------------------------------------------------------------

void print_usage() {
    // print app usage here
    std::cout << "Application usage..." << std::endl;
}

//--------------------------------------------------------------------------------------------------

int main(int argc, char **argv) {
    std::setlocale(LC_ALL, "");

    app_logger _logger(argc, argv);
    if (!_logger.is_valid()) {
        print_usage();
        return 255;
    }

    pgr::logger::get_settings().default_level = _logger.preffered_level();
    pgr::logger::get_settings().callback = std::ref(_logger);

    pgr::def_logger().info() << "Starting...." << ", __cplusplus = " << __cplusplus;

    // regular log messages
    pgr::def_logger().trace() << "It's a trace message";
    pgr::def_logger().debug() << "It's a debug message";
    pgr::def_logger().info() << "It's a information message";
    pgr::def_logger().warning() << "It's a warning message";
    pgr::def_logger().error() << "It's a error message";

    pgr::def_logger().trace("It's a prinf-like %s message", "trace");
    pgr::def_logger().debug("It's a prinf-like %s message", "debug");
    pgr::def_logger().info("It's a prinf-like %s message", "info");
    pgr::def_logger().warning("It's a prinf-like %s message", "warning");
    pgr::def_logger().error("It's a prinf-like %s message", "error");

    pgr::def_logger().check(true, "Checking for %s condition", "true");
    pgr::def_logger().check(false, "Checking for %s condition", "false");

    // categorized log messages
    std::string category = "category_1";

    pgr::log_c(category).trace() << "It's a trace message";
    pgr::log_c(category).debug() << "It's a debug message";
    pgr::log_c(category).info() << "It's a information message";
    pgr::log_c(category).warning() << "It's a warning message";
    pgr::log_c(category).error() << "It's a error message";

    pgr::log_c(category).trace("It's a prinf-like %s message", "trace");
    pgr::log_c(category).debug("It's a prinf-like %s message", "debug");
    pgr::log_c(category).info("It's a prinf-like %s message", "info");
    pgr::log_c(category).warning("It's a prinf-like %s message", "warning");
    pgr::log_c(category).error("It's a prinf-like %s message", "error");

    pgr::log_c(category).check(true, "Checking for %s condition", "true");
    pgr::log_c(category).check(false, "Checking for %s condition", "false");

    // short form of macros
    log_trace() << "It's a trace message in short form of macro";
    log_debug() << "It's a debug message in short form of macro";
    log_info() << "It's a info message in short form of macro";
    log_warning() << "It's a warning message in short form of macro";
    log_error() << "It's a error message in short form of macro";

    log_trace("It's a printf-like %s message in short form of macro", "trace");
    log_debug("It's a printf-like %s message in short form of macro", "debug");
    log_info("It's a printf-like %s message in short form of macro", "info");
    log_warning("It's a printf-like %s message in short form of macro", "warning");
    log_error("It's a printf-like %s message in short form of macro", "error");

    category = "category_2";
    pgr::logger::get_settings().set_category_level(category, pgr::logger::level::error);

    log_trace_c(category) << "It's a trace message in short form of macro";
    log_debug_c(category) << "It's a trace message in short form of macro";
    log_info_c(category) << "It's a trace message in short form of macro";
    log_warning_c(category) << "It's a trace message in short form of macro";
    log_error_c(category) << "It's a trace message in short form of macro";

    log_trace_c(category, "It's a printf-like %s message in short form of macro", "trace");
    log_debug_c(category, "It's a printf-like %s message in short form of macro", "debug");
    log_info_c(category, "It's a printf-like %s message in short form of macro", "info");
    log_warning_c(category, "It's a printf-like %s message in short form of macro", "warning");
    log_error_c(category, "It's a printf-like %s message in short form of macro", "error");

    log_info() << "Printing double PI value with ostreams: " << math::pi();
    log_info("Printing double PI value with printf-like message: %f", math::pi());

    // test containers
    std::vector<std::string> v1{ "A", "B", "C" };
    std::vector<double> v2{ math::pi(), math::pi() * 2, math::pi() * 3, -1 };
    log_info() << "Print vectors: " << v1 << ", " << v2;

    std::set<std::string> s1{ "A", "B", "C" };
    std::set<double> s2{ math::pi(), math::pi() * 2, math::pi() * 3, -1 };
    log_info() << "Print sets: " << s1 << ", " << s2;

    std::map<std::string,double> m{{"A", math::pi()}, {"B",  math::pi() * 2}, {"C", math::pi() *3}};
    log_info() << "Print a map: " << m;

    pgr::def_logger().info() << "Done...";
    return 0;
}
